# Binary Counter

A Pen created on CodePen.io. Original URL: [https://codepen.io/borntofrappe/pen/jOrvzKx](https://codepen.io/borntofrappe/pen/jOrvzKx).

Convert between binary and decimal numbers by toggling a series of checkboxes.

---

For more information refer to [this Github repository](https://github.com/borntofrappe/code/tree/master/Binary%20Numbers).